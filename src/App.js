import React, { Component } from 'react';
import './App.css';
import zyncoder from 'zyncoder';

class App extends Component {
	constructor(props) {
		super(props)
		this.state = this.getInitialState()
	}
	getInitialState = () => {
		return {
			shown: {
				dialog: false
			},
			dec: "",
			zyn: ""
		}
	}
	handleChange = (e) => {
		let v = e.target.value
		if (e.target.name === "plain") {
			this.setState({
				dec: v,
				zyn: zyncoder.enc(v)
			})
		} else if (e.target.name === "zyn") {
			this.setState({
				zyn: v,
				dec: zyncoder.dec(v)
			})
		}
	}
	dumpState = () => {
		console.log(this.state)
	}
	showDialog = () => this.setState({shown: {dialog: true}})
	hideDialog = () => this.setState({shown: {dialog: false}})
	getShown = (str) => {
		let v = ((this.state.shown[str]) ? "" : "dn")
		console.log(this.state, v)
		return v
	}
	shortCircuit = (e) => {
		e.stopPropagation()
	}
	render() {
		return (
			<div className="App">
				<header className="App-header">
					<img className="App-logo" alt="zynzyn" src="https://zyn.pilipali.io/gfx/zynzyn.svg" />
					<h1 onClick={this.showDialog} style={{fontSize: "110%", margin: "0", fontWeight: "normal"}}><em><strong>Zyncoder Live</strong> [¿?]</em></h1>
					<div onClick={this.hideDialog} className={"drk " + this.getShown("dialog")}>
						<div onClick={this.shortCircuit} className="dialog">
						<p>
							Have you ever wanted a custom base64 -like format based on your favorite <a href="https://zyn.pilipali.io" rel="noopener noreferrer" target="_blank">Kazakh song?</a> Worry not, for #zyncoder is here! #zyn #зын #дыкын<br />
							<br /><a href="https://www.npmjs.com/package/zyncoder" rel="noopener noreferrer" target="_blank">JS/NPM package</a>
							<br /><a href="https://packagist.org/packages/meklu/zyncoder" rel="noopener noreferrer" target="_blank">PHP/Composer package</a>
							<br /><br /><a href="https://t.me/addstickers/zynpack" rel="noopener noreferrer" target="_blank">Zyn stickers for Telegram!</a>
						</p>
						</div>
					</div>
					<label>Plain<br /><textarea cols="80" rows="20" autoFocus name="plain" value={this.state.dec} onChange={this.handleChange}></textarea></label>
					<label><textarea name="zyn" cols="80" rows="20" value={this.state.zyn} onChange={this.handleChange}></textarea><br />Zyncoded</label>
				</header>
			</div>
		);
	}
}

export default App;
