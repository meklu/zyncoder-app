# Zyncoder Live

This app allows you to zyncode things with a custom base64 -like system.

This is a live demo app for the zyncoder format. There's an instance running at
https://zyn.pilipali.io/zyncoder/

# WTF?

This particular app depends on the node version of zyncoder, the source of
which can be found at https://gitlab.com/meklu/zyncoder-npm
